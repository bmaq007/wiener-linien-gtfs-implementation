<?php

class GtfsController extends Controller
{

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
            \Yii::log("Starting the Worker Processes", \CLogger::LEVEL_INFO, __METHOD__);
            // File Generation pool
            $workers[] = new Workers(Workers::FILE_ROUTES_TXT);
            $workers[] = new Workers(Workers::FILE_AGENCY_TXT);
            $workers[] = new Workers(Workers::FILE_STOPS_TXT);
            $workers[] = new Workers(Workers::FILE_CALENDAR_TXT);
            $workers[] = new Workers(Workers::FILE_TRIPS_TXT);
            $workers[] = new Workers(Workers::FILE_STOP_TIMES_TXT);
            
             foreach($workers as $worker)
             {
                 $worker->start();
             }
            
             $count = count($workers);

            // Let the threads come back
            foreach (range(0, $count-1) as $i) {
                echo "Worker $i has completed it's assignment <br />";
                $workers[$i]->join();
            }
            
            $worker = new Workers(Workers::FILE_ZIP_OUTPUT);
            $worker->start();
            \Yii::log("Ending the Worker Processes", \CLogger::LEVEL_INFO, __METHOD__);
            
            
            
           // $this->render('index');
	}
        
        public function actionGenerate()
        {
            
        }

}