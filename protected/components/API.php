<?php

/**
 * All Manner of API Logic come in here
 *
 * @author Bolaji Smith
 */
class API
{
    private $master_data;
    
    private $routes;
    
    private $stops;
    
    private $platforms;
    
    private $trips;
    
    
    /**
     * Constructor method for API class
     * @param boolean $generate_data
     * Switch between Generating Master Data or not
     */
    public function __construct($generate_data=false)
    {
        if($generate_data)
        {
            $this->generateMasterData();
            $this->sortMasterData();
        }
    }
    
    /**
     * Generate Master Data
     */
    public function generateMasterData()
    {
        $this->master_data = json_decode(file_get_contents("http://code.clops.at/wiener-linien/"));
    }
    
    /**
     * Sort Master Data into routes and stops and platforms
     * @return NULL
     */
    public function sortMasterData()
    {
        $routes = array();
        
        $stops = array();
        
        $trips = array();
        
        $platforms = array();
        $x=0;
        $y = 0;
        foreach($this->master_data as $data)
        {
            //Stations
            $platforms[$x]['id'] = $data->HALTESTELLEN_ID;
            $platforms[$x]['longitude'] = $data->WGS84_LON;
            $platforms[$x]['latitude'] = $data->WGS84_LAT;
            $platforms[$x]['name']   = $data->NAME;
            $platforms[$x]['DIVA'] = $data->DIVA;
            
            if(isset($data->PLATFORMS))
            {
                foreach($data->PLATFORMS as $inner)
                {
                    //Routes
                    $routes[$y]['route_name'] = $inner->LINIE;
                    $routes[$y]['route_type'] = $inner->VERKEHRSMITTEL;

                    //Stops
                    $stops[$y]['is_realtime'] = $inner->ECHTZEIT;
                    $stops[$y]['rbl_number'] = $inner->RBL_NUMMER;
                    $stops[$y]['stop_code'] = $inner->STEIG;
                    $stops[$y]['stop_longitude'] = $inner->STEIG_WGS84_LON;
                    $stops[$y]['stop_latitude'] = $inner->STEIG_WGS84_LAT;
                    $stops[$y]['stop_name'] = $data->NAME;
                    
                    //Trips
                    $trips[$y]['route_name'] = $inner->LINIE;
                    $trips[$y]['direction'] = $inner->RICHTUNG;
                    $trips[$y]['rbl_number'] = $inner->RBL_NUMMER;
                    $trips[$y]['order'] = $inner->REIHENFOLGE;
                    
                    $y++;
                }
            
                $x++;
            }
        }
        
        $this->platforms = $platforms;
        $this->routes = Helpers::uniqueArray($routes, 'route_name');
        $this->stops = Helpers::uniqueArray($stops, 'rbl_number');
        $this->trips = $trips;
    }
    
    /**
     * fetch Master Data array
     * @return array
     */
    public function getMasterArray()
    {
        return $this->master_data;
    }
    
    /**
     * fetch routes array
     * @return array
     */
    public function getRoutes()
    {
        return $this->routes;
    }
    
    /**
     * fetch stops array
     * @return array
     */
    public function getStops()
    {
        return $this->stops;
    }
    
    /**
     * fetch platforms array
     * @return array
     */
    public function getPlatforms()
    {
        return $this->platforms;
    }
    
    /**
     * fetch trips array
     * @return array
     */
    public function getTrips()
    {
        return $this->trips;
    }
    
}

