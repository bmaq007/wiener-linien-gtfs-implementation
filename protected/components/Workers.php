<?php
/**
 * Worker Threads for GTFS Generation
 *
 * @author Bolaji Smith
 */
class Workers extends \Thread 
{
    private $file_to_generate;
    
    const FILE_AGENCY_TXT = 'agency.txt';
    
    const FILE_CALENDAR_DATES_TXT = 'calendar_dates.txt';
    
    const FILE_CALENDAR_TXT = 'calendar.txt';
    
    const FILE_TRIPS_TXT = 'trips.txt';
    
    const FILE_ROUTES_TXT = 'routes.txt';
    
    const FILE_STOP_TIMES_TXT = 'stop_times.txt';
    
    const FILE_STOPS_TXT = 'stops.txt';
    
    const FILE_ZIP_OUTPUT = "zip_output";
    
    private $helper_object;
    
    private $content_generator_object;
    
    
    
    public function __construct($file_to_generate) 
    {
        \Yii::log("Generated the Worker process for $file_to_generate", \CLogger::LEVEL_INFO, __METHOD__);
        $this->file_to_generate = $file_to_generate;
        
        $this->helper_object = new Helpers();
        $this->content_generator_object = new GTFSFileContentGenerator();
    }
    
    public function run()
    {
        switch($this->file_to_generate)
        {
            case(self::FILE_AGENCY_TXT):
            {
                $this->generateAgencyTxt();
                break;
            }
            case(self::FILE_CALENDAR_DATES_TXT):
            {
                $this->generateCalendarDatesTxt();
                break;
            }
            
            case(self::FILE_CALENDAR_TXT):
            {
                $this->generateCalendarTxt();
                break;
            }
            
            case(self::FILE_TRIPS_TXT):
            {
                $this->generateTripsTxt();
                break;
            }
            
            case(self::FILE_ROUTES_TXT):
            {
                $this->generateRoutesTxt();
                break;
            }
            
            case(self::FILE_STOPS_TXT):
            {
                $this->generateStopsTxt();
                break;
            }
            
            case(self::FILE_STOP_TIMES_TXT):
            {
                $this->generateStopTimesTxt();
                break;
            }
            
            case(self::FILE_ZIP_OUTPUT):
            {
                $this->generateZip();
                break;
            }
        }
    }
    
    private function generateAgencyTxt()
    {
        $this->helper_object->createCSVFile($this->content_generator_object->generateAgencyContent(), self::FILE_AGENCY_TXT);
        
        \Yii::log("Generate Agency Txt Done", \CLogger::LEVEL_INFO, __METHOD__);
    }
    
    private static function generateCalendarDatesTxt()
    {
        $file_name = self::FILE_CALENDAR_DATES_TXT;
         \Yii::log("Generate Calendar Dates Txt Done", \CLogger::LEVEL_INFO, __METHOD__);
    }
    
    private function generateCalendarTxt()
    {
        $this->helper_object->createCSVFile($this->content_generator_object->generateCalendarContent(), self::FILE_CALENDAR_TXT);
        
        \Yii::log("Generate Calendar Txt Done", \CLogger::LEVEL_INFO, __METHOD__);
    }
    
    private function generateTripsTxt()
    {
        $this->helper_object->createCSVFile($this->content_generator_object->generateTripsContent(), self::FILE_TRIPS_TXT);
        
        \Yii::log("Generate Trips Txt Done", \CLogger::LEVEL_INFO, __METHOD__);
    }
    
    private function generateRoutesTxt()
    {
        $this->helper_object->createCSVFile($this->content_generator_object->generateRoutesContent(), self::FILE_ROUTES_TXT);
        
        \Yii::log("Generate Routes Txt Done", \CLogger::LEVEL_INFO, __METHOD__);
    }    
    private function generateStopsTxt()
    {
        $this->helper_object->createCSVFile($this->content_generator_object->generateStopsContent(), self::FILE_STOPS_TXT);
        
        \Yii::log("Generate Stops Txt Done", \CLogger::LEVEL_INFO, __METHOD__);
    }
    
    private function generateStopTimesTxt()
    {
        $this->helper_object->createCSVFile($this->content_generator_object->generateStopTimesContent(), self::FILE_STOP_TIMES_TXT);
        
        \Yii::log("Generate stop_times Txt Done", \CLogger::LEVEL_INFO, __METHOD__);
    }
    
    private function generateZip()
    {
        \Yii::log("Generating Zip", \CLogger::LEVEL_INFO, __METHOD__);
        //This would be the zip file name
        $file_name = "wienerlinien_gtfs".date('Ymd').".zip";
        $directory = ROOT_PATH ."/cache";
        \Yii::log("Calling the helper function", \CLogger::LEVEL_INFO, __METHOD__);
        $this->helper_object->createZipFromDirectoryContent($file_name, $directory);
        
    }
}