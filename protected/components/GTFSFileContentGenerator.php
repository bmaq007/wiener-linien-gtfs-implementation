<?php
/**
 * Generate GTFS Content
 *
 * @author Bolaji Smith
 */
 class GTFSFileContentGenerator
{
     private $api;

     public function __construct()
     {
        $this->api = new API(true);
     }
    
    /**
     * GEnerate Agency.txt content
     * @return array
     */
    public function generateAgencyContent()
    {
         Yii::log("Generate Agency Txt Content", CLogger::LEVEL_INFO, __METHOD__);
        $content[] = ["agency_id","agency_name","agency_url","agency_timezone","agency_lang","agency_phone","agency_fare_url"];
        $content[] = [0,"Wiener Linien","http://www.wienerlinien.at","Europe/Vienna","de","+43-(01)-790-90",""];
        return $content;
    }
    
    /**
     * Generate Calendar.txt content
     * @return array
     */
    public function generateCalendarContent()
    {
        Yii::log("Generate Calendar Txt Content", CLogger::LEVEL_INFO, __METHOD__);
        $calendar[] = ["service_id","monday","tuesday","wednesday","thursday","friday","saturday","sunday","start_date","end_date"];
        $year = date("Y");
        $calendar[] = [1,1,1,1,1,1,1,1,$year."0101",$year."1231"];
        return $calendar;
    }
    
    public function generateStopTimesContent()
    {
        $stop_times[] = ["trip_id", 'arrival_time', "departure_time", "stop_id", "stop_sequence", "pickup_type", "drop_off_type"];
        $stop_times[] = [1,"0:06:10","0:06:10",977,1,0,0];
        $stop_times[] = [1,"","",2550,2,0,1];
        $stop_times[] = [2,"0:06:20","0:06:30",1443,3,0,0];
        $stop_times[] = [2,"","",123,4,0,0];
        
        return $stop_times;
    }
    
    /**
     * Generate Stops.txt content
     * @return array
     */
    public function generateStopsContent()
    {
        $stops = new StopsObject;
        return $stops->getStopsContent($this->api->getStops());
    }
    
   /**
    * Generates Routes.txt content
    * @return array
    */ 
    public function generateRoutesContent()
    {
        $routes = new RoutesObject;
        return $routes->getRoutesContent($this->api->getRoutes());
    }
    
    /**
     * Generate Trips.txt
     * @return array
     */
    public function generateTripsContent()
    {
        $trips = new TripsObject();
        return $trips->getTripsContent($this->api->getTrips());
    }
    
}

/**
 * Route Object
 *
 * @author Bolaji Smith
 */
class RoutesObject {
    
    const VEHICLE_TRAM = "ptTram";
    
    const VEHICLE_NIGHT_BUS = "ptBusNight";
    
    const VEHICLE_TRAIN_S = "ptTrainS";
    
    const VEHICLE_METRO = "ptMetro";
    
    const VEHICLE_TRAM_VRT = "ptTramVRT";
    
    const VEHICLE_BUS_CITY = "ptBusCity";
    
    /**
     * Generate Routes from API Master Data
     * @param array $route_data
     * The array returned from the GetRoutes in API
     * @return array
     */
    public function getRoutesContent(array $route_data)
    {
        $routes = array();
        $routes[] = ["route_id", "agency_id", "route_short_name", "route_long_name", "route_desc", "route_type", "route_url", "route_color", "route_text_color"];
        
        foreach($route_data as $value)
        {
            $route_id = $value['route_name'];
            $route_short_name = $value['route_name'];
            $route_long_name = "";
            $route_desc = "";
            $route_type = $this->getRouteType($value['route_type']);
            $route_url = "";
            $route_color = "";
            $route_text_color = "";
            $routes[] = [$route_id, 0, $route_short_name, $route_long_name, $route_desc, $route_type,$route_url, $route_color, $route_text_color ];
        }
        return $routes;
    }
    
    /**
     * Get GTFS route_type
     * @param string $means_of_transportation
     * @return int
     */
    private function getRouteType($means_of_transportation)
    {
        switch($means_of_transportation)
        {
            case(self::VEHICLE_TRAM):
            case(self::VEHICLE_TRAM_VRT):
            {return 0;}
            
            case(self::VEHICLE_METRO):
            { return 1;}
            
            case(self::VEHICLE_TRAIN_S):
            {return 2;}
            
            case(self::VEHICLE_NIGHT_BUS):
            case(self::VEHICLE_BUS_CITY):
            {return 3;}
        }
    }
}


/**
 * Stops Object
 *
 * @author Bolaji Smith
 */
class StopsObject {
    
    /**
     * Generate Stops from API Master Data
     * @param array $stop_data
     * The array returned from the GetRoutes in API
     * @return array
     */
    public function getStopsContent(array $stop_data)
    {
        $stops[] = ["stop_id", "stop_code", "stop_name", "stop_desc", "stop_lat", "stop_lon", "zone_id", "stop_url", "location_type", "parent_station", "stop_timezone", "wheelchair_boarding"];
        $x = 1;
        foreach($stop_data as $value)
        {
            $stop_id = $value['rbl_number'];
            $stop_code = $value['stop_code'];
            $stop_name = $value['stop_name'];
            $stop_desc = "";
            $stop_lat = $value['stop_latitude'];
            $stop_lon = $value['stop_longitude'];
            $zone_id = "";
            $stop_url = "";
            $location_type = "";
            $parent_station = "";
            $stop_timezone = "";
            $wheelchair_boarding = "";
            
            $stops[] = [$stop_id, $stop_code, $stop_name, $stop_desc, $stop_lat, $stop_lon, $zone_id, $stop_url, $location_type, $parent_station, $stop_timezone, $wheelchair_boarding];
            $x++;
        }
        return $stops;
    }
}


/**
 * Trips Object
 * 
 * @author Bolaji Smith
 */
class TripsObject {
    
    /**
     * Generate Trips from API Master Data
     * @param array $trip_data
     * The array returned from the GetTrips in API
     * @return array
     */
    public function getTripsContent(array $trip_data)
    {
        $trips[] = ["route_id", "service_id", "trip_id", "trip_headsign", "trip_short_name", "direction_id", "block_id", "shape_id", "wheelchair_accessible", "bikes_allowed"];
        $x = 1;
        foreach($trip_data as $value)
        {
            $route_id = $value['route_name'];
            $service_id = 1;
            $trip_id = $x;
            $direction_id = self::getDirectionID($value['direction']);
                    
            $trips[] = [$route_id, $service_id, $trip_id, "", "", $direction_id, "", "", "", ""];
            $x++;
        }
        return $trips;
    }
    /**
     * Get Direction ID
     * @param string $direction
     * @return int
     */
    private static function getDirectionID($direction)
    {
        switch($direction)
        {
            case("H"):
            {return 0;}
            case(("R")):
            {return 1;}
        }
        
    }
}

class stopTimes
{
    public function generateStopTimes($platforms_data)
    {
        foreach($platforms_data as $data)
        {
            $query = self::generateStopTimesXMLQuery($data->DIVA);
            $url = "http://webservice.qando.at/2.0/webservice.ft";
            $xml_response = Helpers::sendXMLRequest($url, $query);
        }
    }
    
    private static function generateStopTimesXMLQuery($DIVA)
    {
        return '<ft> 
                    <request clientId="123" apiName="api_get_monitor" apiVersion="2.0"> 
                        <client clientId="123"/> 
                        <requestType>api_get_monitor</requestType> 
                        <monitor> 
                            <outputCoords>WGS84</outputCoords> 
                            <type>stop</type> 
                            <name>'.$DIVA.'</name> 
                            <line></line> 
                            <sourceFrom>stoplist</sourceFrom> 
                        </monitor> 
                    </request> 
                 </ft> ';
    }
}

