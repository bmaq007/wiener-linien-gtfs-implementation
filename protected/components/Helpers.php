<?php
/**
 * Collection of Helpers Functions
 *
 * @author Bolaji Smith
 */
class Helpers {
    
    /**
     * Create a CSV file from an array
     * 
     * @param array $data
     * The array data to be written to file
     * @param string $filename
     * The file name of the file
     */
    public static function createCSVFile(array $data, $filename)
    {
        $directory = ROOT_PATH ."/cache/";
        $filename = $directory.$filename;
        $csv_string = self::writeCSV($data);
        $new_file_name = tempnam($directory, 'preparing_');
	file_put_contents($new_file_name, $csv_string);
        rename($new_file_name, $filename);
        chmod($filename, 0644);
        return NULL;
    }
    
    /**
     * Create Zip File from Directory Content
     * @param string $file_name
     * Name of Output Zip File
     * @param type $source
     * Directory to Zip
     * @param type $save_to
     * Location to save the zip file to
     * 
     * return NULL
     */
    public static function createZipFromDirectoryContent($file_name, $source, $save_to=NULL)
    {
        if(!(is_dir($save_to)))
        {
            $save_to = ROOT_PATH ."/zip/";
        }
        
        $destination = $save_to.$file_name;
        self::Zip($source, $destination);
    }
    
    
    /**
     * Create a CSV string from an input array
     * 
     * @param array $array 
     * Input Array
     * @param char $delimiter
     * The CSV delimiter
     * @param char $enclosure
     * The String Enclosure for the CSV
     * 
     * @return string
     */
    private static function writeCSV($array, $delimiter=",")
    {
        $fp = fopen('php://temp', 'r+');
        foreach ($array as $file) 
        {
            $result = [];
            array_walk_recursive($file, function($item) use (&$result) 
            {
                $result[] = $item;
            });
            fputcsv($fp, $result, $delimiter);
            if(0 === fseek($fp, -1, SEEK_CUR)) {
            fwrite($fp, PHP_EOL);
          }
        }
        rewind($fp);
        $data = fread($fp, 10240000);
        fclose($fp);
        return rtrim($data, PHP_EOL);
    }
    
    /**
     * Perform recursive archive of a source folder to a destination file
     * @param type $source
     * @param type $destination
     * @return boolean
     */
    private static function Zip($source, $destination)
    {
        if (!extension_loaded('zip') || !file_exists($source)) {
            return false;
        }

        $zip = new ZipArchive();
        if (!$zip->open($destination, ZIPARCHIVE::CREATE)) {
            return false;
        }

        $source = str_replace('\\', '/', realpath($source));
        
        if (is_dir($source) === true)
        {
            $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($source), RecursiveIteratorIterator::SELF_FIRST);

            foreach ($files as $file)
            {
                $file = str_replace('\\', '/', $file);
                
                // Ignore "." and ".." folders
                if( in_array(substr($file, strrpos($file, '/')+1), array('.', '..')) )
                    continue;

                $file = realpath($file);
                
                if (is_dir($file) === true)
                {
                    $zip->addEmptyDir(str_replace($source . '/', '', $file . '/'));
                }
                else if (is_file($file) === true)
                {
                    $zip->addFromString(basename($file), file_get_contents($file));
                }
            }
            exit();
        }
        else if (is_file($source) === true)
        {
            $zip->addFromString(basename($source), file_get_contents($source));
        }

        return $zip->close();
    }
    
    
    public static function uniqueArray($array, $column)
    {
        $result = [];
	for($i = 0; $i < sizeof($array); $i++)
        {
            if (!array_key_exists($array[$i][$column], $result))
            {
                $result[$array[$i][$column]] = $array[$i];
            }
        }
	return $result;
    }    
    
    public static function sendXMLRequest($url, $request)
    {
        $headers = array(
                "Content-type: text/xml;charset=\"utf-8\"",
                "Accept: text/xml",
                "Cache-Control: no-cache",
                "Pragma: no-cache",
                "Content-length: ".strlen($request),
            ); 

        return simplexml_load_string(self::sendCURLRequest($url, $request, $headers));
    }
    
    public static function sendCURLRequest($url, $params, $headers=NULL)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params); // the SOAP request
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        // converting
        $response = curl_exec($ch); 
        curl_close($ch);
       return $response;
    }
    
}